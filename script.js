// Récupérer les données des fichiers XML
const clientData = fetchingData("client.xml");
const localiteData = fetchingData("localite.xml");
const conditionTaxationData = fetchingData("conditiontaxation.xml");
const tarifData = fetchingData("tarif.xml");

function fetchingData(nameOfFile) {
  const data = fetch(nameOfFile)
    .then((response) => response.text())
    .then((str) => new window.DOMParser().parseFromString(str, "text/xml"));
  return data;
}

// Attendre que toutes les promesses soient résolues
Promise.all([clientData, localiteData, conditionTaxationData, tarifData]).then(
  (values) => {
    const clientXml = values[0];
    const localiteXml = values[1];
    const conditionTaxationXml = values[2];
    const tarifXml = values[3];

    // Récupérer les éléments HTML
    const expediteurSelect = document.getElementById("expediteur");
    const destinataireSelect = document.getElementById("destinataire");
    const nombreColisInput = document.getElementById("nombreColis");
    const poidsInput = document.getElementById("poids");
    const transportPayeurSelect = document.getElementById("transportPayeur");
    const calculerButton = document.querySelector("button");
    const resultDiv = document.getElementById("result");

    // Initialiser les variables clients, localites , tarifs et conditions
    const clients = Array.from(clientXml.querySelectorAll("ObjectClient")).map(
      (client) => {
        return {
          idClient: client.querySelector("idClient").textContent,
          raisonSociale: client.querySelector("raisonSociale").textContent,
          codePostal: client.querySelector("codePostal").textContent,
          ville: client.querySelector("ville").textContent,
          heritage: client.querySelector("idClientHeritage")
            ? client.querySelector("idClientHeritage").textContent
            : null,
        };
      }
    );

    const localites = Array.from(
      localiteXml.querySelectorAll("ObjectLocalite")
    ).map((localite) => {
      return {
        codePostal: localite.querySelector("codePostal").textContent,
        ville: localite.querySelector("ville").textContent,
        zone: localite.querySelector("zone").textContent,
      };
    });

    const tarifs = Array.from(tarifXml.querySelectorAll("ObjectTarif")).map(
      (tarif) => {
        return {
          idClient: tarif.querySelector("idClient").textContent,
          idClientHeritage: tarif.querySelector("idClientHeritage")
            ? tarif.querySelector("idClientHeritage").textContent
            : null,
          codeDepartement: tarif.querySelector("codeDepartement").textContent,
          zone: tarif.querySelector("zone").textContent,
          montant: parseFloat(tarif.querySelector("montant").textContent),
        };
      }
    );

    const conditiontaxations = Array.from(
      conditionTaxationXml.querySelectorAll("ObjectConditionTaxation")
    ).map((conditon) => {
      return {
        idClient: conditon.querySelector("idClient").textContent,
        taxePortDu: parseFloat(
          conditon.querySelector("taxePortDu").textContent
        ),
        taxePortPaye: parseFloat(
          conditon.querySelector("taxePortPaye").textContent
        ),
        useTaxePortDuGenerale: conditon.querySelector("useTaxePortDuGenerale")
          .textContent,
        useTaxePortPayeGenerale: conditon.querySelector(
          "useTaxePortPayeGenerale"
        ).textContent,
      };
    });
    // Remplir la liste déroulante d'expéditeurs
    clients.forEach((client) => {
      const option = document.createElement("option");
      option.value = client.idClient;
      option.text = client.codePostal + " " + client.ville;
      expediteurSelect.appendChild(option);
    });

    // Remplir la liste déroulante de destinataires
    clients.forEach((client) => {
      const option = document.createElement("option");
      option.value = client.idClient;
      option.text = client.codePostal + " " + client.ville;
      destinataireSelect.appendChild(option);
    });

    //recupérer la data d'un client apartir de son idClient
    function getClientData(idclient) {
      const client = clients.find((client) => client.idClient === idclient);
      return client;
    }
    //recupérer les informations du client qui va payer
    function getTransportPayeurData(destinataire, expediteur, transportPayeur) {
      let transportPayeurData;
      if (transportPayeur === "destinataire") {
        transportPayeurData = getClientData(destinataire);
      } else {
        transportPayeurData = getClientData(expediteur);
      }
      return transportPayeurData;
    }
    // Fonction pour récupérer le tarif à utiliser
    function getTarifToUse(tarifs, clientId, codePostal, zone) {
      let tarifHeritage = "";
      let tarif = tarifs.find(
        (t) =>
          t.idClient === clientId &&
          t.zone === zone &&
          t.codeDepartement === codePostal
      );
      // console.log(clientId);
      // console.log(codePostal);
      // console.log(zone);
      if (!tarif) return null;
      if (tarif && tarif.idClientHeritage) {
        tarifHeritage = tarifs.find(
          (t) => tarif.idClientHeritage === t.idClientHeritage
        );
      }
      if (tarifHeritage) {
        return tarifHeritage.montant;
      }
      return tarif.montant;
    }
    //Calculer le tarif hors taxe
    function calculerMontantHT() {
      // Récupérer les valeurs des champs du formulaire
      const destinataire = destinataireSelect.value;
      const transportPayeur = transportPayeurSelect.value;
      // Récupérer les informations du client destinataire
      const destinataireData = clients.find(
        (client) => client.idClient === destinataire
      );

      // Récupérer la localité du destinataire
      const localiteDataForDestinataire = localites.find(
        (localite) => localite.ville === destinataireData.ville
      );

      console.log(localiteDataForDestinataire);

      if (!localiteDataForDestinataire) {
        console.error(
          "Aucune donnée de localité trouvée pour le code postal:",
          destinataireData.codePostal
        );
        return;
      }

      // Récupérer le tarif correspondant à la localité du destinataire
      let tarif = getTarifToUse(
        tarifs,
        destinataire,
        destinataireData.codePostal,
        localiteDataForDestinataire.zone
      );

      if (!tarif) {
        const zonePrecedente = (
          parseInt(localiteDataForDestinataire.zone) - 1
        ).toString();
        tarif = getTarifToUse(
          tarifs,
          destinataire,
          destinataireData.codePostal,
          zonePrecedente
        );
      }

      // Si le tarif n'existe toujours pas, utiliser le tarif général
      if (!tarif) {
        tarif = getTarifToUse(
          tarifs,
          "0",
          destinataireData.codePostal,
          localiteDataForDestinataire.zone
        );
      }

      // Si aucun tarif n'a été trouvé, afficher une erreur et arrêter la fonction
      if (!tarif) {
        console.error("Aucun tarif trouvé pour le calcul");
      }
      return tarif;
    }

    //Calculer le montant taxation
    function calculMontantTaxation() {
      const destinataire = destinataireSelect.value;
      const transportPayeur = transportPayeurSelect.value;
      const expediteur = expediteurSelect.value;
      let transportPayeurData;
      transportPayeurData = getTransportPayeurData(
        destinataire,
        expediteur,
        transportPayeur
      );
      console.log(transportPayeurData);
      for (let conditionTaxation of conditiontaxations) {
        if (
          conditionTaxation.idClient === transportPayeurData.idClient &&
          conditionTaxation.useTaxePortDuGenerale === false &&
          conditionTaxation.useTaxePortPayeGenerale === false
        ) {
          if (transportPayeur === "expediteur") {
            return conditionTaxation.taxePortPaye;
          }
          if (transportPayeur === "destinataire") {
            return conditionTaxation.taxePortDu;
          }
        }
        if (
          conditionTaxation.idClient === transportPayeurData.idClient &&
          conditionTaxation.useTaxePortDuGenerale === true &&
          transportPayeur === "destinataire"
        ) {
          return conditiontaxations[0].taxePortDu;
        }
        if (
          conditionTaxation.idClient === transportPayeurData.idClient &&
          conditionTaxation.useTaxePortPayeGenerale === true &&
          transportPayeur === "expediteur"
        ) {
          return conditiontaxations[0].taxePortPaye;
        }
      }
      // Si un client ne possède pas de condition de taxation, alors on utilise les conditions de
      //taxation générales.
      if (transportPayeurData.idClient === expediteur) {
        return conditiontaxations[0].taxePortPaye;
      }
      //ici on peut juste  mettre return conditiontaxations[0].taxePortDu;
      if (transportPayeurData.idClient === destinataire) {
        return conditiontaxations[0].taxePortDu;
      }
    }
    function calculMontantTransport(e) {
      let montantTaxation = calculMontantTaxation();

      let tarif = calculerMontantHT();
      console.log("tarif: " + tarif);
      let montantTransport = 0;
      montantTransport = montantTaxation + tarif;
      // Afficher le résultat
      resultDiv.innerHTML =
        "Montant HT : " +
        tarif +
        " €" +
        "</br>" +
        " taxe à appliquer : " +
        montantTaxation +
        " €" +
        "</br>" +
        "Montant Transport:" +
        montantTransport.toString() +
        "€";
      e.preventDefault();
    }
    // Ajouter l'événement de clic sur le bouton de calcul
    calculerButton.addEventListener("click", calculMontantTransport);
  }
);
